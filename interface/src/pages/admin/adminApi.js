import httpAxios from '@/public/http.js';
import './adminApiMock.js';

export default {
  /**
   * 管理员登陆表单提交
   * @param form
   */
   /**
  * showdoc
  * @catalog 管理后台接口/用户接口
  * @title 用户登录
  * @description 用户登录的接口
  * @method get
  * @url[hostname] / do /api_user_login
  * @param account 必选 string 账户
  * @param password 必选 string 密码  
  * @return {
        "error_code":": 0,
        "data": {
          "uid": "1",
          "username": "12154545",
          "name": "吴系挂",
          "groupid": 2,
          "reg_time": "1436864169",
          "last_login_time": "0"
        }
      }
  * @return_param groupid int 用户组id
  * @return_param name string 用户昵称
  * @remark 这里是备注信息
  * @number 99
  */
  async adminLoginSub(form) {
    httpAxios.get('do/api_user_login', {
      params: {
        account: form.account || '',
        password: form.password || ''
      }
    }).then(res => {
      if (res.code === 1) {
        //  登录成功,返回管理员信息
        window.$store.commit('NOTICE_SUCCESS', res.msg);
        window.$store.dispatch('adminLoginSuccess', res.data);
        return true;
      } else {
        window.$store.commit('NOTICE_ERROR', res.msg);
        return false;
      }
    })
  },
  /**
   * 管理员用户快速登录
   * @param admin
   */
  async adminLoginSubLight(admin) {
    httpAxios.get('do/api_user_login_light', {
      params: {
        account: admin.account,
      }
    }).then(res => {
      if (res.code === 1) {
        window.$store.commit("NOTICE_SUCCESS", res.msg);
        window.$store.dispatch('adminLoginSuccess', res.data);
        return true;
      } else {
        window.$store.commit('NOTICE_ERROR', res.msg);
        return false;
      }
    })
  },
  /**
   * 用户注册表单提交
   * @param form
   */
  async userRegisterSub(form) {
    // console.log(form);
    httpAxios.post('do/api_user_register', form
    ).then(response => {
      if (response.code === 1) {
        //  注册成功
        window.$store.commit('NOTICE_SUCCESS', response.msg);
      } else {
        window.$store.commit('NOTICE_ERROR', response.msg);
      }
    })
  },
  /**
   * 获取后台菜单
   */
  async getAdminDashboardMenu() {
    return await httpAxios.get('do/api_admin_menu', {
      params: {
        uid: window.$store.getters.adminUid || null
      }
    }).then(response => {
      // console.log(response);
      if (response.code === 1) {
        window.$store.dispatch('adminMenuInit', response.data);
      }
    })
  },
  async getAdminProfileMenu() {
    httpAxios.get('do/api_admin_profile_menu', {
      params: {
        uid: window.$store.getters.adminUid || null
      }
    }).then(response => {
      if (response.code === 1) {
        window.$store.dispatch('getAdminProfileMenu', response.data);
      }
      return response;
    })
  },
  /**
   * 获取系统配置项
   */
  async getSystemConfiguration() {
    httpAxios.get('do/api_system_configuration', {
      params: {}
    }).then(response => {
      if (response.code === 1) {
        window.$store.dispatch('systemConfigs', response.data);
      }
    })
  },
}
