# admin_thinkphp

#### 项目介绍
> 本项目是基于Thinkphp5.1 + Vue全家桶制作的管理系统

#### 软件架构
`整体结构`

```markdown
+admin_thikphp              根目录
|   +-- application         应用目录
|   +-- config              全局配置
|   +-- extend              外部引用目录
|   +-- public              公共库(访问目录)
|   +-- route               路由目录
|   +-- sql                 模拟数据目录
|   +-- thinkphp            thinkphp5.1核心
|   +-- vendor              第三方库目录
|   +-- interface       后台前端页面项目目录
```

 `技术要求`
> 服务器运行环境
> * PHP >=  7.1.0
> * MySql >= 5.6.0
> * Server >= apache 2.4.0
> * Thinkphp >= 5.1.0
> * Workman >= 2.0
 
 `因为使用了.htaccess重定向文件,如使用其他服务器可自行处理`

> 前端页面运行环境
> * Node.js >= 6.9.0
> * Vue >= 2.6.0
> * Webpack >= 3.4.0
#### 安装教程

本程序以 thinkphp5.1 版本为核心的综合性后台系统;
部署前请使用

* <code>#composer install</code>安装依赖;

> ### Thinkphp 5.1 + swagger-php
> 本项目已经加入 swagger-php 进行 api 文档编写使用一下命令即可生成 swagger.json 文件
>* <code>>>> php ./vendor/zircote/swagger-php/bin/swagger ./application -o ./swagger-doc </code>

> ### Workman Socks + think-worker
> 本项目已采用Workman作为socks通信服务层,得益于thinkphp体系的think-worker模块
> > * 采用composer安装think-worker模块<br>
> > <code>>>> composer require topthink/think-worker</code></br>
>
> > * php启用think worker模块<br>
> > <code>>>> php think worker</code><br>
> > 启用模块后默认访问路径为http://localhost:2346
>
> > * 使用Thinkphp内置类即可
> > <code>>>> use think\worker\Server;</code><br>


#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 dev-[your_name] 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)