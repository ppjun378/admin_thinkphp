<?php
/**
 * Created by PhpStom.
 * User: ppjun378
 * Date: 2018/8/27
 * Time: 下午3:38
 * FileName: 慈善模型文件
 */

namespace app\inforward\model;

use app\inforward\middleware\base\mwModelBase;
use think\Model;

class CharityModel extends Model
{
    use mwModelBase;
    //设置当前模型对应的完整数据库表名称
    protected $connection = 'db_user_center';
    protected $table = 'charity';

}

?>