<?php
/**
 * Create by PhpStorm.
 * User: ppjun378
 * Data: 2018/8/27
 * Time: 下午3:44
 * FileName: 慈善接口文件
 */

namespace app\inforward\apiHandler;

use app\inforward\model\CharityModel;
use think\Exception;

trait CharityApiHandler
{
    /**
     * 接收访问公益 
     * @param $datas
     * showdoc
     * @catelog 前端接口/公益/慈善添加
     * @title 用于接收浏览统计
     * @description 用户接收用户数据
     * @method post
     * @url [hostname]/inforward/admin/api/do/api_charity
     * @return_param code 操作成功返回1
     * @number 99
     * @return {"code":1,"msg":"成功为慈善贡献了力量","data":true,"url":"","wait":3}
     */
    public function api_charity($datas)
    {
        try {
            if  (isset($datas['id']))
                unset($datas['id']);

            // $datas = array(
            //         'user_token' => 'abc',
            //         'money' => 0.2,
            //     );
            $datas['money'] = 0.2;
            $user_token = $datas['user_token'];

            if(empty($user_token)){
                $this->error('token不能为空');
            }

            $charityModel = new CharityModel();

            $result = $charityModel->allowField(true)->save($datas);

            $successMsg = '成功为慈善贡献了力量';
            // $this->success($successMsg,'',$result);
            
        } catch (Exception $exception) {
            $errorMsg = '';
            $this->error($errorMsg ?? $exception->getMessage(), '', ['msg' => $exception->getMessage()]);
        }
    }

    /**
     * @param $datas
     * showdoc
     * @return {"code":1,"msg":"成功获取公益统计信息","data":['money_count':111,'people_count':111],"url":"","wait":3}
     * @catalog 前端接口/公益慈善
     * @title 获取最近慈善统计
     * @description 用于获取公益慈善统计内容
     * @method get
     * @url [hostname]/inforward/admin/api/do/api_get_charity_count
     * @remark 这里是备注信息
     * @number 99
     */
    public function api_get_charity_count($datas)
    {
        try {
            $charityModel = new CharityModel();
            // 金额总数
            $result['money_count'] = $charityModel->order('create_time','desc')->sum('money');
            // 参与人数
            $result['people_count'] = $charityModel->order('create_time','desc')->count('id');

            $successMsg = '数据获取成功';
            $this->success($successMsg,'',$result);

        } catch (Exception $exception) {
            $errorMsg = '数据获取失败';
            $this->error($errorMsg ?? $exception->getMessage(), '', ['msg' => $exception->getMessage()]);
        }

    }
    
}

?>